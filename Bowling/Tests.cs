﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Bowling
{
    class Tests
    {
        private Game game;

        [SetUp]
        public void CreateGame()
        {
            game = new Game();
        }

        [Test]
        public void CanPlayGutterGame()
        {
            RollGame(timesRolled: 20, score: 0);

            Assert.AreEqual(0, game.Score());
        }

        [Test]
        public void CanRollOnes()
        {
            RollGame(timesRolled: 20, score: 1);

            Assert.AreEqual(20, game.Score());
        }

        [Test]
        public void RollingScore()
        {
            RollGame(timesRolled: 5, score: 3);

            Assert.AreEqual(15, game.Score());
        }

        [Test]
        public void CanRollSpares()
        {
            RollGame(5, 3);

            Assert.AreEqual(20, game.Score());
        }

        [Test]
        public void CanRollStrikes()
        {
            RollGame(10, 3);

            Assert.AreEqual(30 + 20 + 10, game.Score());
        }

        [Test]
        public void PerfectGame()
        {
            RollGame(score: 10, timesRolled: 12);

            Assert.AreEqual(300, game.Score());
        }

        private void RollGame(int score, int timesRolled)
        {
            for (int i = 0; i < timesRolled; i++)
            {
                game.Roll(score);
            }
        }
    }
}
