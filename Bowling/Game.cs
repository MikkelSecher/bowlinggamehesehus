﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling
{
    public class Game
    {
        private int rollCounter = 0;
        private List<Frame> scoreBoard = new List<Frame>() { new Frame() };

        internal void Roll(int pinsDown)
        {
            if (scoreBoard.Last().FrameFull() && scoreBoard.Count() < 10)
            {
                var newFrame = new Frame();
                scoreBoard.Last().NextFrame = newFrame;
                scoreBoard.Add(newFrame);
            }

            scoreBoard.Last().Roll(pinsDown);

            rollCounter++;
        }

        internal int Score()
        {
            return scoreBoard.Sum(x => x.Score());
        }
    }

    public class Frame
    {
        public List<int> Rolls = new List<int>();

        public Frame NextFrame { get; set; }

        public bool FrameFull()
        {
            if (Rolls.Count == 1 && Rolls[0] == 10)
                return true;

            return IsSpare() || Rolls.Count == 2;
        }

        internal void Roll(int pinsDown)
        {
            Rolls.Add(pinsDown);
        }

        internal int Score()
        {
            if (IsStrike())
                return 10 + ScoreOfNextTwoRolls();

            if (IsSpare())
               return Rolls.Sum() + ScoreOfNextRoll();

            return Rolls.Sum();
        }

        public int ScoreOfNextTwoRolls()
        {
            if (Rolls.Count == 2)
            {
                return Rolls[1];
            }
            if (Rolls.Count == 3)
            {

                var score = Rolls[1] + Rolls[2];
                if (Rolls[1] == 10)
                {
                    score += 10;
                }
                return score;
            }
            if(NextFrame != null)
            {
                if (NextFrame.Rolls.Count == 2)
                    return NextFrame.Rolls[0] + NextFrame.Rolls[1];

                if (NextFrame.IsStrike())
                    return 10 + NextFrame.ScoreOfNextRoll();
            }

            return 0;
        }

        public int ScoreOfNextRoll()
        {
            if(NextFrame != null)
            {
                return NextFrame.Rolls[0];
            }

            return 0;
        }

        public bool IsSpare()
        {
            return 
                Rolls.Count == 2 
                && Rolls[0] + Rolls[1] == 10;
        }

        public bool IsStrike()
        {
            return Rolls.Any() && Rolls[0] == 10;
        }
    }
}
